# oracle-fmw-sysv-init
## By Gene Liverman, 2013

### Project description
This project contains init scripts for starting the various parts of Oracle Fusion Middleware.  
This includes all the parts that are used in an installation of Ellucian's Internet Native Banner
and Self Service Banner.  The scritps are in SysV format designed for Red Hat Enterprise Linux 5.  
A spec file is also included that can be used to build an RPM which will then allow the resulting 
package to be managed via yum.

### RPM Description
Oracle does not ship SysV init scripts with its Fusion Middleware product.  This package aims
to fill the gap that is left by this so that the different parts of it can be started and stopped like 
other services on the system. These scripts also take into account the ordering of startup and 
shutdown and how that relates to users of PowerBroker® Identity Services, Open Edition in 
that the shutdown ordering is designed to stop all FMW services prior to PBIS shutting down. 
This is important if your "oracle" user is actually an Active Directory account.
