#!/bin/sh
#
# oracle_wls_reports Oracle Weblogic Reports
#
# chkconfig:   	345 99 1
# description: 	Oracle Weblogic Reports \
# 		The script needs to be saved as /etc/init.d/oracle_wls_reports \
#		and then issue chkconfig --add oracle_wls_reports as root

### BEGIN INIT INFO
# Provides: oracle_wls_reports
# Required-Start: $network $local_fs $remote_fs $syslog
# Required-Stop: $network $local_fs $remote_fs $syslog
# Should-Start: oracle_opmn oracle_adminsrv oracle_nodemgr oracle_wls_forms
# Should-Stop: oracle_opmn oracle_adminsrv oracle_nodemgr oracle_wls_forms
# Default-Start: 3 4 5
# Default-Stop: 0 1 2 6
# Short-Description: Oracle Weblogic Reports.
# Description: Starts and stops Oracle Weblogic Reports.
### END INIT INFO

. /etc/rc.d/init.d/functions

FILE_NAME=`/usr/bin/readlink $0`

if [ $FILE_NAME ]; then
        SERVICE_NAME=`/bin/basename $FILE_NAME`
else
        SERVICE_NAME=`/bin/basename $0`
fi

LOCKFILE="/var/lock/subsys/$SERVICE_NAME"

[ -e /etc/sysconfig/oracle_mwcommon ] && . /etc/sysconfig/oracle_mwcommon
[ -e /etc/sysconfig/$SERVICE_NAME ] && . /etc/sysconfig/$SERVICE_NAME

RETVAL=0

start() {
        OLDPID=`/usr/bin/pgrep -f "$PROCESS_STRING" -P 1`
        if [ ! -z "$OLDPID" ]; then
            echo "$SERVICE_NAME is already running (pid $OLDPID) !"
            exit
        fi

        echo -n $"Starting $SERVICE_NAME: "
		/bin/su $DAEMON_USER -c "$PROGRAMSTART 2>&1 |logger -t $SERVICE_NAME &"

        RETVAL=$?
        [ $RETVAL -eq 0 ] && echo_success

        echo
        [ $RETVAL -eq 0 ] && touch $LOCKFILE
}

stop() {
        echo -n $"Stopping $SERVICE_NAME: "
        OLDPID=`/usr/bin/pgrep -f "$PROCESS_STRING" -P 1`
        if [ "$OLDPID" != "" ]; then
            /bin/su $DAEMON_USER -c "$PROGRAMSTOP 2>&1 |logger -t $SERVICE_NAME"
        else
            /bin/echo "$SERVICE_NAME is stopped"
        fi

        RETVAL=$?
        [ $RETVAL -eq 0 ] && echo_success

        echo
        [ $RETVAL -eq 0 ] && rm -f $LOCKFILE

}

restart() {
        stop
        sleep 10
        start
}

case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart|force-reload|reload)
        restart
        ;;
  condrestart|try-restart)
        [ -f $LOCKFILE ] && restart
        ;;
  status)
        OLDPID=`/usr/bin/pgrep -f "$PROCESS_STRING" -P 1`
        if [ "$OLDPID" != "" ]; then
            /bin/echo "$SERVICE_NAME is running (pid: $OLDPID)"
        else
            /bin/echo "$SERVICE_NAME is stopped"
        fi
        RETVAL=$?
        ;;
  *)
        echo $"Usage: $0 {start|stop|status|restart|reload|force-reload|condrestart}"
        exit 1
esac

exit $RETVAL

