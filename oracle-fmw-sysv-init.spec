Name:		oracle-fmw-sysv-init
Version:	1.0.1
Release:	1%{?dist}
Summary:	A set of SysV init scripts and configuration files for Oracle Fusion Middleware
Group:		Applications/System
License:	BSD
URL:		https://bitbucket.org/genebean/oracle-fmw-sysv-init
Source0:	https://bitbucket.org/genebean/oracle-fmw-sysv-init/downloads/%{name}-%{version}.tar.gz
BuildArch:	noarch
BuildRoot:	%{_topdir}/BUILDROOT/

BuildRequires:		coreutils
Requires:		/bin/logger /usr/bin/pgrep
Requires(post):		chkconfig
Requires(postun):	chkconfig

%description
Oracle does not ship SysV init scripts with its Fusion Middleware product.  This package aims
to fill the gap that is left by this so that the different parts of it can be started and stopped like 
other services on the system. These scripts also take into account the ordering of startup and 
shutdown and how that relates to users of PowerBroker® Identity Services, Open Edition in 
that the shutdown ordering is designed to stop all FMW services prior to PBIS shutting down. 
This is important if your "oracle" user is actually an Active Directory account.


%prep
%setup -q


%build


%install
rm -rf %{buildroot}
mkdir -p -m 755 %{buildroot}/etc/rc.d/init.d
mkdir -p -m 755 %{buildroot}/etc/sysconfig
install -m 755 etc/rc.d/init.d/oracle_* %{buildroot}/etc/rc.d/init.d/
install -m 644 etc/sysconfig/oracle_* %{buildroot}/etc/sysconfig/


%post
# Cleanup existing entries
/sbin/chkconfig --del oracle_opmn 2> /dev/null
/sbin/chkconfig --del oracle_adminsrv 2> /dev/null
/sbin/chkconfig --del oracle_nodemgr 2> /dev/null
/sbin/chkconfig --del oracle_wls_forms 2> /dev/null
/sbin/chkconfig --del oracle_wls_reports 2> /dev/null

# Add the current version of the init script
/sbin/chkconfig --add oracle_opmn
/sbin/chkconfig --add oracle_adminsrv
/sbin/chkconfig --add oracle_nodemgr
/sbin/chkconfig --add oracle_wls_forms
/sbin/chkconfig --add oracle_wls_reports


%postun
/sbin/chkconfig --del oracle_opmn 2> /dev/null
/sbin/chkconfig --del oracle_adminsrv 2> /dev/null
/sbin/chkconfig --del oracle_nodemgr 2> /dev/null
/sbin/chkconfig --del oracle_wls_forms 2> /dev/null
/sbin/chkconfig --del oracle_wls_reports 2> /dev/null


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LICENSE README.md
%config(noreplace) /etc/sysconfig/oracle_mwcommon
%config(noreplace) /etc/sysconfig/oracle_opmn
%config(noreplace) /etc/sysconfig/oracle_adminsrv
%config(noreplace) /etc/sysconfig/oracle_nodemgr
%config(noreplace) /etc/sysconfig/oracle_wls_forms
%config(noreplace) /etc/sysconfig/oracle_wls_reports
/etc/rc.d/init.d/oracle_opmn
/etc/rc.d/init.d/oracle_adminsrv
/etc/rc.d/init.d/oracle_nodemgr
/etc/rc.d/init.d/oracle_wls_forms
/etc/rc.d/init.d/oracle_wls_reports


%changelog
* Wed Sep 25 2013 Gene Liverman <gliverma@westga.edu> - 1.0.1-1
- Bumped version for a bug fix in config files

* Mon Sep 23 2013 Gene Liverman <gliverma@westga.edu> - 1.0.0-2
- Changed dependency list to specify what is actually required as opposed to a
  package that might provide the needed program.

* Fri Sep 20 2013 Gene Liverman <gliverma@westga.edu> - 1.0.0-1
- Initial version of oracle-fmw-sysv-init.spec


